const axios = require('axios')
const $ = require('cheerio')

const endpoint = 'https://seever.com/enzoknol'

axios.get(endpoint)
    .then((docs) => {
        const social = {}
        const filterted = $.load(docs.data)
        const new_filter = filterted('body > script')
        new_filter
            .map((i , x) => {
                if(x.children[0] === undefined){
                    return 
                }
                else{
                    const item = JSON.parse(x.children[0].data)['props']["pageProps"]
                    const profileData= JSON.parse(item["profileData"])["channels"]

                    // here for youtube username, browser's script return youtube user id
                    // there is 2 things we can do here
                    // either call youtube v3.0 API, and get user detail with ID
                    // or access `https://www.youtube.com/channel/${userId}` and scrape username there.
                    social["username_tiktok"] =  profileData["TikTok"],
                    social["username_youtube"] = profileData["YouTube"]
                    social["username_instagram"] = profileData["Instagram"]
                }
            })


        console.log(`[+] Successfully fetched Username from ${endpoint}`)
        console.log("************************************************")
        console.log(`[+] Instagram Username: @${social["username_instagram"]} `)
        console.log(`[+] Youtube Username: @${social["username_youtube"]} `)
        console.log(`[+] Tiktok Username: @${social["username_tiktok"]} `)

        // makeYoutubeRequest(social["username_youtube"])
        
    })
    .catch((e) => {
        console.log(`[+] There was an ${e.message} while scraping data from ${endpoint}`);
    })
 
function makeYoutubeRequest(userId){
    axios.get(`https://www.youtube.com/channel/${userId}`)
        .then((docs) => {
            const filtered = $.load(docs.data)
            console.log(docs.data)
            const new_filter = filtered('body > ytd-app > div')
            
            new_filter
            .map((i , x) => {
                console.log(x.children[0]["children"])
            })
        })
}